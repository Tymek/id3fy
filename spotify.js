const request = require('request')
const msToTime = require('./msToTime')
const {
  applySpec,
  compose,
  map,
  path,
  prop,
} = require('ramda')
require('dotenv').config()

const user = process.env.CLIENT_ID
const pass = process.env.CLIENT_SECRET
let token = null

const getCreds = () => new Promise((resolve, reject) => {
  request.post(
    `https://accounts.spotify.com/api/token`,
    {
      auth: {
        user,
        pass,
      },
      form: {
        grant_type: 'client_credentials',
      },
      json: true,
    },
    (err, httpResponse, body) => {
      if (err) reject(err)

      token = body.access_token
      resolve(body)
    },
  )
})

const search = q => new Promise(async (resolve, reject) => {
  if (!token) {
    await getCreds()
  }

  request.get(
    'https://api.spotify.com/v1/search',
    {
      auth: {
        bearer: token,
      },
      qs: {
        q,
        type: 'track',
        include_external: true,
      },
      json: true,
    },
    (err, httpResponse, body) => {
      if (err) reject(err)

      resolve(body)
    },
  )
})

const getArtists = compose(
  map(prop('name')),
  prop('artists'),
)

const getInfo = applySpec({
  name: prop('name'),
  artists: getArtists,
  album: path(['album', 'name']),
  track_number: prop('track_number'),
  total_tracks: path(['album', 'total_tracks']),
  disc_number: prop('disc_number'),
  release_date: path(['album', 'release_date']),
  album_artists: compose(
    getArtists,
    prop('album'),
  ),
  spotify_uri: prop('uri'),
  spotify_url: path(['external_urls', 'spotify']),
  popularity: prop('popularity'),
  explicit: prop('explicit'),
  duration: compose(
    msToTime,
    prop('duration_ms'),
  )
})

module.exports = q => search(q).then(
  compose(
    getInfo,
    path(['tracks', 'items', 0]),
  )
)
