const chalk = require('chalk')
const NodeID3 = require('node-id3')

const read = async (file) => new Promise((resolve, reject) => {
  NodeID3.read(file, function(err, tags) {
    if (err) {
      console.error(chalk.red('Error reading tags'), file, err)
      return reject(err)
    }

    console.log(chalk.green('Read tags'), file, tags)
    return resolve(tags)
  })  
})

const update = async (file, tags) => new Promise((resolve, reject) => {
  NodeID3.update(tags, file, (err) => {
    if (err) {
      console.error(chalk.red('Error updating tags'), file, err)
      return reject(err)
    }
    
    console.log(chalk.green('Updated tags'), file, tags)
    return resolve(tags)
  })
})

module.exports = {
  read,
  update,
}
