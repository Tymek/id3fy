const fs = require('fs')
const spotify = require('./spotify')
const id3 = require('./id3tag')
const {
  compose,
  filter,
  test,
  map,
} = require('ramda')

require('dotenv').config()

const dir = process.env.TARGET_DIR

const tags = {
  trackNumber: 101,
}

// id3.read()
spotify('').then(console.log)

const files = filter(test(/.+\.mp3$/), fs.readdirSync(dir))

// map(
//   compose(
//     // f => id3.update(f, { ITUNESADVISORY: 1 }),
//     id3.read,
//     f => `${dir}${f}`,
//   ),
// )(files)

// console.log(files)
