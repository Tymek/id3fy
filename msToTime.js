/**
 * @see https://coderwall.com/p/wkdefg/converting-milliseconds-to-hh-mm-ss-mmm
 */
function msToTime(duration) {
  const milliseconds = parseInt((duration%1000))
  const seconds = parseInt((duration/1000)%60)
  const minutes = parseInt((duration/(1000*60)))

  return `${minutes}:${seconds}.${milliseconds}`;
}

module.exports = msToTime
